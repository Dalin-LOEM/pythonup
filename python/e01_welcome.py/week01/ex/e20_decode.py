
import string

def is_lower(value):
  if(ord(value.lower()) == ord(value)):
    return True

  return False

simpleInput = input("Enter a secret message: \n >> ")
if(len(simpleInput) == 0):
  print("Noting to encode")
  exit


def decode_rot13(value):
  range_a_to_z = string.ascii_lowercase[:27]
  encrypted_message = ""

  for char in simpleInput:
    SECRET_KEY      = 13
    tmp_char        = char.lower()
    encrypted_char  = ""

    if(range_a_to_z.index(tmp_char) - SECRET_KEY < 0):
      encrypted_char += range_a_to_z[range_a_to_z.index(tmp_char) - SECRET_KEY + 26]
    else:
      encrypted_char += range_a_to_z[range_a_to_z.index(tmp_char) - SECRET_KEY]

    if(not is_lower(char)):
      encrypted_message += encrypted_char.upper()
    else:
      encrypted_message += encrypted_char

  return encrypted_message

print(decode_rot13(simpleInput))