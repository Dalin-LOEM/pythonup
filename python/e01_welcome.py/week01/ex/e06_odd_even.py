def isOdd(value):
    if(value.isnumeric() and int(value) % 2 == 0):
        return True

def isEven(value):
    if(value.isnumeric() and int(value) % 2 != 0):
        return True
        
while True:
    f_input = input("Enter a number \n >>> ")
    if(isEven(f_input)):
        print(f_input, "is Even")
    elif(isOdd(f_input)):
        print(f_input, "is Odd")
    elif(f_input == "EXIT"):
        break
    else:
        print(f_input , "is not a valid number")