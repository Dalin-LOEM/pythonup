# import library date time
from datetime import datetime

# give an input to user to input the date time
date = input("Enter a your date: \n >>> ")

# after validate the input if it is a string so go to else
if (date == "abc"):
    # if string equal abc then print 
    print("Your timestamp is not valid")
elif(date == ""):
    print("")
else:
    # convert string to integer number 
    time_stamp = int(date)
    # else print the date time stamp that we set
    print(datetime.utcfromtimestamp(time_stamp).strftime('%Y-%m-%d %H:%M:%S'))
