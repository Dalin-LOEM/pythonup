# call library datetime
from datetime import datetime

# get current date for today with the time
today = datetime.today()

# after print it
print(today)

# from datetime import datetime
# timestamp = 1623646780
# date_time = datetime.fromtimestamp(timestamp)
# print("Date time object:", date_time)