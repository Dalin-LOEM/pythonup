# call library date time
from datetime import datetime

# create object to call current time
now = datetime.now()

# then set it to string time
current_time = now.strftime("%H:%M:%S")

# after print it
print("current time is:", current_time)