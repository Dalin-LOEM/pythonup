dict_count = ['c','a','a','a','b','b','b','b','c','c']

# for this we use to count the value that has the same value
result = {
    num:dict_count.count(num) for num in dict_count
}

if(result == ""):
    print("Your string is empty.")
else:
     # Converting into list of tuple 
    list = [(k, v) for k, v in result.items()] 

    # Printing list of tuple 
    print(list) 