# my list
my_list = ([4, 2, 19, 50, 49, 48, 1, 2, 3, 4, 5])

# sort the my list and reverse it to true
my_list.sort(reverse=True)

# print my list with reverse list
print('My List: ', my_list)

# second list with shorted and reverse function
second_list = {'b', 'a', 'a', 'a', 'a', 'a', 'a', 'b'}

print(sorted(second_list, reverse=True))