# for this way we can print all of the string and integer
list_num_str = {'abc', 'def', 'def', -1, 2, 3, '-123', '888', 'hello'}

# for this part we sort int value
intList=sorted([i for i in list_num_str if type(i) is int])

# for this part we sort int value
strList=sorted([i for i in list_num_str if type(i) is str])

# so we can print it
print(strList)