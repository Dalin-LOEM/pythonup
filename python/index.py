print("hello")

CONDITION = False
CONDITION_ELSE = True

if CONDITION: 
    print("good job")
if CONDITION_ELSE: 
    print("Python")
else:
    print("ELSE")